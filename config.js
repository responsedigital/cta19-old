module.exports = {
    'name'  : 'CTA 19',
    'camel' : 'Cta19',
    'slug'  : 'cta-19',
    'dob'   : 'CTA_19_1440',
    'desc'  : 'A contact section with address, email, phone, and social icons. Also includes a contact form.',
}