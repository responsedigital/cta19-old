class Cta19 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initCta19()
    }

    initCta19 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: CTA 19")
    }

}

window.customElements.define('fir-cta-19', Cta19, { extends: 'div' })
