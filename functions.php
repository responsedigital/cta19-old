<?php

namespace Fir\Pinecones\Cta19;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Cta19',
            'label' => 'Pinecone: CTA 19',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A contact section with address, email, phone, and social icons. Also includes a contact form."
                ],
                [
                    'label' => 'Content',
                    'name' => 'contentTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Address',
                    'name' => 'address',
                    'type' => 'textarea',
                    'new_lines' => 'br'
                ],
                [
                    'label' => 'Phone',
                    'name' => 'phone',
                    'type' => 'text'
                ],
                [
                    'label' => 'Email',
                    'name' => 'email',
                    'type' => 'text'
                ],
                [
                    'label' => 'Social',
                    'name' => 'socialTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Facebook',
                    'name' => 'facebook',
                    'type' => 'link'
                ],
                [
                    'label' => 'Twitter',
                    'name' => 'twitter',
                    'type' => 'link'
                ],
                [
                    'label' => 'Instagram',
                    'name' => 'instagram',
                    'type' => 'link'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
